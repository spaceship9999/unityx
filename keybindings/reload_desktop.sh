pkill -9 polybar; sleep 1 && fish -c 'polybar -c design/panel.cfg default' &
pkill -9 conky; sleep 1 && fish -c 'for widget in ./design/widgets/*.conf; conky -c $widget &; end' &
